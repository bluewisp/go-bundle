package modbundle

import (
	"fmt"
)

var bundles map[string]*Bundle

// Bundle contains the flat data from the given file
type Bundle struct {
	Data map[interface{}]interface{}
}

// GetBundle returns a configured bundle based on key name
func GetBundle(key string) *Bundle {
	return bundles[key]
}

// RemoveBundle removes a bundle from its registry
func RemoveBundle(key string) {
	delete(bundles, key)
}

// GetString get a string typed value from a bundle
func (b *Bundle) GetString(path string, args ...interface{}) string {
	if i, err := b.Get(path, args...); err == nil {
		return i.(string)
	}
	return ""
}

// GetBool get a boolean typed value from a bundle
func (b *Bundle) GetBool(path string, args ...interface{}) bool {
	if b, err := b.Get(path, args...); err == nil {
		return b.(bool)
	}
	return false
}

// GetInt get a int typed value from a bundle
func (b *Bundle) GetInt(path string, args ...interface{}) int {
	if i, err := b.Get(path, args...); err == nil {
		return i.(int)
	}
	return 0
}

// GetFloat get a float typed value from a bundle
func (b *Bundle) GetFloat(path string, args ...interface{}) float64 {
	if f, err := b.Get(path, args...); err == nil {
		return f.(float64)
	}
	return 0.00
}

// Get a value from a bundle with args
func (b *Bundle) Get(path string, args ...interface{}) (interface{}, error) {
	message, ok := b.Data[path]
	if !ok {
		return nil, fmt.Errorf("%s is missing in bundle", path)
	}
	if message, ok := message.(string); ok {
		if len(args) == 0 {
			return message, nil
		}
		return fmt.Sprintf(message, args...), nil
	}

	return message, nil
}
